<?php
/**
 * Created by PhpStorm.
 * User: taoln
 * Date: 12/25/14
 * Time: 1:56 PM
 */

?>

<?php
    include $_SERVER['DOCUMENT_ROOT']."/blog/models/BaseModels.php";
    require $_SERVER['DOCUMENT_ROOT']."/blog/models/article.php";
    include $_SERVER['DOCUMENT_ROOT']."/blog/models/user.php";
    require $_SERVER['DOCUMENT_ROOT']."/blog/server/token.php";

    function post(){
        $article = new Article;
        $user = new User();
        // collect value of input field
        $articlePost                   = array();
        $articlePost['title']          = $_POST['title'];
        $articlePost['description']    = $_POST['description'];
        $articlePost['userId']         = $_POST['userId'];
        $articlePost['param']          = $_POST['param'];

        $idUpdate                      = array();
        $idUpdate['articleId']         = $_POST['articleId'];

        $token                          = array();
        $token['_rememberToken']        = $_POST['_Token'];

        if (_validate($user,$token)==true){
            if (empty($idUpdate)){
                if (empty($articlePost['title'])) {
                    return 0;
                } else {
                    if(empty($articlePost['param'])){
                        return 0;
                    }else{
                        if(empty($articlePost['description'])){
                            return 0;
                        }else{
                            if(empty($articlePost['userId'])){
                                return 0;
                            }else{
                                $article->post($articlePost);
                                exit;
                            }
                        }
                    }
                }
            }else{
                $article->update($articlePost,$idUpdate);
                exit;
            }
        }else{
            echo "Login de post";exit;
        }

    }

    function get(){
        $article = new Article;
        $parameter = $_REQUEST;
        if(!empty($parameter['articleId'])){
            $post = $article->get($parameter);
        }
        else{
            $post = " article nay ko co trong data";
        }
        print_r(json_encode($post));
        exit;
    }
/*
http://localhost/blog/server/article.php?articleId=3' or articleId='29


*/
    function delete(){
        $article = new Article;
        $user = new User();

        $token                          = array();
        $token['_rememberToken']        = $_POST['_Token'];
       var_dump($_POST);

        if (_validate($user,$token)){
            $parameter = $_REQUEST;
            if(!empty($parameter['articleId'])){
                $article->delete($parameter);
            }else{
                echo "Khong dung Id";
            }
        }else{
            echo"Login de delete";
        }
    }



    switch ($_SERVER["REQUEST_METHOD"]) {
        case "POST":
            post();
            break;
        case "GET":
            get();
            break;
        case "DELETE":
            delete();
            break;
        default:
            echo "Your favorite color is neither red, blue, or green!";
    }
?>