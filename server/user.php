<?php
/**
 * Created by PhpStorm.
 * User: taoln
 * Date: 12/29/14
 * Time: 10:52 AM
 */
?>

<?php
    include $_SERVER['DOCUMENT_ROOT']."/blog/models/BaseModels.php";
    require $_SERVER['DOCUMENT_ROOT']."/blog/models/article.php";
    include $_SERVER['DOCUMENT_ROOT']."/blog/models/user.php";
    require $_SERVER['DOCUMENT_ROOT']."/blog/server/token.php";

    function post(){

        $user = new User();
        // collect value of input field
        $user_post                   = array();
        $user_post['username']       = $_POST['username'];
        $user_post['password']       = $_POST['password'];
        $user_post['_rememberToken'] = $_POST['_rememberToken'];

        $token = _generate_random_string();
        $user->update($token,$user_post['username']);

        $id_update                      = array();
        $id_update['articleId']         = $_POST['articleId'];

        $token                          = array();
        $token['_rememberToken']        = $_POST['_Token'];

        if (_validate($user,$token)){
            if (empty($colum_update)){
                if (empty($article_post['title'])) {
                    return 0;
                } else {
                    if(empty($article_post['param'])){
                        return 0;
                    }else{
                        if(empty($article_post['description'])){
                            return 0;
                        }else{
                            if(empty($article_post['userId'])){
                                return 0;
                            }else{
                                $article->post($article_post);
                                exit;
                            }
                        }
                    }
                }
            }else{
                $article->update($article_post,$id_update);
                exit;
            }
        }else{
            echo "Login de post";
        }

    }

    /*
     * get Article
     * http://localhost/blog/server/article.php?articleId=28
     * http://localhost/blog/server/article.php?articleId=3' or articleId='29
     */
    function get(){
        $article = new Article;
        $parameter = $_REQUEST;
        if(!empty($parameter['articleId'])){
            $post = $article->get($parameter);
        }
        else{
            $post = " article nay ko co trong data";
        }
        print_r(json_encode($post));
        exit;
    }

    /*
     * delele Article
     * http://localhost/blog/server/article.php?articleId=33
     * http://localhost/blog/server/article.php?articleId=3' or articleId='29

    */
    function delete(){
        $user                           = new User();
        $article                        = new Article;

        $parameter                      = $_REQUEST;
        
        $token                          = array();
        $token['_rememberToken']        = $_POST['_Token'];

        if (_validate($user,$token)){
            if(!empty($parameter['articleId'])){
                $article->delete($parameter);
            }else{
                echo "Khong dung Id";
            }
        }

    }



    switch ($_SERVER["REQUEST_METHOD"]) {
        case "POST":
            post();
            break;
        case "GET":
            get();
            break;
        case "DELETE":
            delete();
            break;
        default:
            echo "something method";
    }
?>