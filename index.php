<?php
/**
 * Created by PhpStorm.
 * User: taoln
 * Date: 12/24/14
 * Time: 4:57 PM
 */
?>


<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Home page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link type="text/css" rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="./stylesheets/style.css">

    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://malsup.github.com/jquery.cycle.all.js"></script>
    <script src="./js/script.js"></script>
</head>
<body>
<header class="header">
    <div class="container-fluid wrapper">
        <nav class="navbar nav nav-pills">
            <ul class="nav nav-tabs">
                <li><a href="/blog/">Home</a></li>
                <li><a href="/blog/public/user/">User</a></li>
                <li><a href="/blog/public/article/">Blog</a></li>
                <li><a href="#">LogIn</a></li>
                <li><a href="#">LogOut</a></li>
            </ul>
        </nav>
    </div>
</header><!--/ header-->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">.col-md-8</div>
        <div class="col-md-4">.col-md-4</div>
    </div>
</div>

</body>
</html>
