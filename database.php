<?php
/**
 * Created by PhpStorm.
 * User: taoln
 * Date: 12/24/14
 * Time: 3:26 PM
 */

class Data{

   protected function DB($sql){
        include 'config.php';
        $username           =   $data_config['database_use'] ;
        $servername         =   $data_config['database_link'];
        $database_name      =   $data_config['database_name'];
        $password           =   $data_config['database_password'];

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database_name", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           /* echo "Connected successfully";*/
            return $conn->exec($sql);
            /*echo "Query success.";*/
        }
        catch(PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
       $conn = null;
       die();
    }
    protected function DB2($sql){
        include 'config.php';
        $username           =   $data_config['database_use'] ;
        $servername         =   $data_config['database_link'];
        $database_name      =   $data_config['database_name'];
        $password           =   $data_config['database_password'];

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database_name", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stm = $conn->prepare($sql);
            $stm->execute();
            $result = $stm->setFetchMode(PDO::FETCH_ASSOC);
            $result=$stm->fetchAll();
            return $result;
            /*echo "Query success.";*/
        }
        catch(PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
        $conn = null;
        die();
    }
}

?>