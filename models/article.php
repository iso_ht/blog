<?php
/**
 * Created by PhpStorm.
 * User: taoln
 * Date: 12/25/14
 * Time: 10:06 AM
 */

/*include $_SERVER['DOCUMENT_ROOT']."/blog/models/BaseModels.php";*/


class Article extends BasePostModels {

    protected $table = 'article';

    /*add new article*/
    public function post($article_post){
        $this->BasePost($article_post);
    }

    /*delete article*/
    public function delete($article_id){
        $this->BaseDelete($article_id);
    }

    /*get article*/
    public function get($value){
        return $this->BaseGet($value);
    }

    /*update article*/
    public function update($article_post,$colum_update){
        $this->BaseUpdate($article_post,$colum_update);
    }
}

?>